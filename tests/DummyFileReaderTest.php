<?php

namespace Tests;

final class DummyFileReaderTest extends TestCase
{
    public function provider()
    {
        return [
            [__DIR__.'/assets/file_that_does_not_exists.txt',[],[], \LoveToKnow\Exception\FileNotFoundException::class],
            [__DIR__.'/assets/empty.txt',[''],[__DIR__.'/assets/empty.txt' => ['']]],
            [__DIR__.'/assets/C.txt',[10,2],[__DIR__.'/assets/C.txt' => [10,2]]],
        ];
    }

    /**
     * @dataProvider provider
     */
    public function testAssignmentExample(string $filePath, array $results, $dummyData = [], $exception = null,)
    {
        if($exception) {
            $this->expectException($exception);
        }

        $fileReader = new \LoveToKnow\DummyFileReader($dummyData);
        foreach($fileReader->readLine($filePath) as $key => $result){
            $this->assertEquals($results[$key],$result);
        }
    }
}