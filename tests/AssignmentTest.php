<?php

namespace Tests;

use LoveToKnow\FileReader;
use LoveToKnow\FileSummation;
use LoveToKnow\Result;

final class AssignmentTest extends TestCase
{
    public function dataProvider()
    {
        return [
            [__DIR__.'/assets/A.txt',111, "A.txt - 111\nB.txt - 39\nC.txt - 12\n"],
            [__DIR__.'/assets/B.txt', 39, "B.txt - 39\nC.txt - 12\n"],
            [__DIR__.'/assets/C.txt', 12, "C.txt - 12\n"],
            [__DIR__.'/assets/D.txt', 0, '', \LoveToKnow\Exception\CircularReferenceException::class],
            [__DIR__.'/assets/file_that_does_not_exists.txt', 0, '', \LoveToKnow\Exception\FileNotFoundException::class],
            [__DIR__.'/assets/empty.txt', 0, '', \LoveToKnow\Exception\IncorrectValueException::class],
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testAssignmentExample(string $filePath, int $expectedTotal, $expectedOutput = '', $exception = null, )
    {
        if($exception) {
            $this->expectException($exception);
        }
        $fileSummation = new FileSummation(new FileReader());
        $result = $fileSummation->sum($filePath);

        $this->assertInstanceOf(Result::class,$result);
        $this->assertEquals(basename($filePath),$result->getFilename());
        $this->assertEquals($expectedTotal,$result->getTotal());
        $this->assertEquals($expectedOutput,(string)$fileSummation->sum($filePath));
    }
}