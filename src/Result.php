<?php


namespace LoveToKnow;


class Result
{
    private string $filename = '';
    private int $total = 0;

    public function __construct(string $filename)
    {
        $this->filename = trim($filename);
    }

    /**
     * @var Result[]
     */
    private array $files = [];

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function increaseTotal(int $value): self {
        $this->total += $value;
        return $this;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function addFile(Result $result): self {
        $this->files[] = $result;
        $this->increaseTotal($result->getTotal());
        return $this;
    }

    public function __toString(): string
    {
        return $this->getFilename().' - '.$this->getTotal()."\n".implode("\n", $this->files);
    }
}