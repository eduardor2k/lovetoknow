<?php

namespace LoveToKnow;

class FileReader implements FileReaderInterface
{
    protected int $lengh = 4096;

    public function __construct(int $length = 4096) {
        $this->lengh = $length;
    }

    public function readLine(string $filePath): iterable
    {
        if(!file_exists($filePath)){
            throw new Exception\FileNotFoundException(sprintf('File "%s" not found', $filePath));
        }

        $file = fopen($filePath, 'r');

        while (!feof($file)) {
            yield fgets($file, $this->lengh);
        }

        fclose($file);
        return;
    }
}