<?php


namespace LoveToKnow;


class DummyFileReader implements FileReaderInterface
{
    protected array $data = [];

    public function __construct(array $dummyData) {
        $this->data = $dummyData;
    }

    public function readLine(string $filePath): iterable
    {
        if(!isset($this->data[$filePath])){
            throw new Exception\FileNotFoundException(sprintf('File "%s" not found', $filePath));
        }

        foreach($this->data[$filePath] as $row){
            yield $row;
        }
        return;
    }
}