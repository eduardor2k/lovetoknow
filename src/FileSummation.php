<?php


namespace LoveToKnow;


use LoveToKnow\Exception\CircularReferenceException;
use LoveToKnow\Exception\IncorrectValueException;

class FileSummation
{
    private FileReaderInterface $fileReader;

    public function __construct(FileReaderInterface $fileReader)
    {
        $this->fileReader = $fileReader;
    }

    /**
     * @throws CircularReferenceException
     * @throws IncorrectValueException
     * @throws Exception\FileNotFoundException
     */
    public function sum(string $filePath, array $filesAlreadyRead = []): Result
    {
        if(in_array($filePath, $filesAlreadyRead)) {
            throw new CircularReferenceException(
                sprintf('Circular reference detected %s', implode('->', $filesAlreadyRead)));
        }

        $filesAlreadyRead[] = $filePath;

        $sum = new Result(basename($filePath));
        foreach ($this->fileReader->readLine(trim($filePath)) as $line => $value) {

            if(intval($value) == $value && trim($value) != ''){
                $sum->increaseTotal($value);
                continue;
            }

            if(is_string($value) && trim($value) != ''){
                $sum->addFile($this->sum(dirname($filePath).DIRECTORY_SEPARATOR.$value, $filesAlreadyRead));
                continue;
            }

            throw new IncorrectValueException(
                sprintf('Unexpected value in file "%s" at line "%s" with value "%s" ', $filePath, $line, $value));
        }
        return $sum;
    }
}