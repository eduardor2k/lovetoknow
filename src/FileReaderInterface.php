<?php


namespace LoveToKnow;

interface FileReaderInterface
{
    /**
     * @throws Exception\FileNotFoundException
     * @param string $filePath
     * @return iterable
     */
    public function readLine(string $filePath): iterable;
}